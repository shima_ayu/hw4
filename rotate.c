#include <stdio.h>
#include<string.h>
#define SIZE 5
char data[SIZE];
int k;

main(){
  int i;
  strcpy(data, "abcde");
  printf("please input integer\n k=");
  fflush(stdout);
  scanf("%d",&k);
  printf("before");
  show_array();
  rotate_array();
  //Test
  if(k==1 && data[0]=='e' && data[1]=='a' && data[2]=='b' && data[3]=='c' && data[4]=='d'){
     printf("success!!\nafter"); 
     show_array(); 
  }else if(k==1 && (data[0]!='e' || data[1]!='a' || data[2]!='b' || data[3]!='c' || data[4]!='d')){
    printf("False!\n");
    }
}

show_array(){
  int i;
  printf("[");
  for (i = 0; i < SIZE; i++){
    printf("%c ",data[i]);
  }
  printf("]\n");
}

rotate_array(){
  int tmp[SIZE], i, j;
  if(k<0){//ｋが負なら以下の処理をしておく
    while(k<0){
    k+=SIZE;
    }
  }
  
  for (i = 0; i < SIZE; i++) {//i=0から配列の要素数SIZEまで繰り返し
    j = i - k;//jにi-kを代入
    if (j < 0){//jが負になったら
      j += SIZE; //ｊにSIZEをたす
    } 
    tmp[i] = data[j];
  }
  for (i = 0; i < SIZE; i++){
    data[i] = tmp[i];
  }
  
  return 0;
}
